const slides = document.getElementsByClassName("carousel-item");
const texts = document.getElementsByClassName("title");
const carouselPrevBtn = document.getElementById("carousel-btn-prev");
const carouselNextBtn = document.getElementById("carousel-btn-next");
const bullets = document.getElementsByClassName("carousel-bullet")

const textContent = ["The Joker", "Baby Driver", "Parasite"];

const totalSlides = slides.length;
let slidePosition = 0;

carouselPrevBtn.addEventListener("click", moveToPrevSlide);
carouselNextBtn.addEventListener("click", moveToNextSlide);

setInterval(moveToNextSlide, 5000);
setText(slidePosition);

function moveToPrevSlide() {
  hideAllSlides();
  if (slidePosition === 0) {
    slidePosition = 2;
  } else {
    slidePosition--;
  }
  showSlides(slidePosition);
  setText(slidePosition);
}

function moveToNextSlide() {
  hideAllSlides();
  if (slidePosition === totalSlides - 1) {
    slidePosition = 0;
  } else {
    slidePosition++;
  }
  showSlides(slidePosition);
  setText(slidePosition);
  setBullet(slidePosition);
}

function hideAllSlides() {
  for (let slide of slides) {
    slide.classList.remove("carousel-item-visible");
    slide.classList.add("carousel-item-hidden");
  }
  for (let bullet of bullets) {
    bullet.classList.remove("active")
  }
}

function showSlides(slidePosition) {
    slides[slidePosition].classList.add("carousel-item-visible");
    bullets[slidePosition].classList.add("active");
}

function setText(slidePosition) {
    for (let text of texts) {
        text.textContent = textContent[slidePosition];
    }
}

function setBullet(slidePosition) {
  
        
}

